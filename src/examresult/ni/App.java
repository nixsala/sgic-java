package examresult.ni;

public class App {

	public static void main(String[] args) {
		SubjectService subjectStore=new SubjectServiceImpl();
		subjectStore.addSubject(new Subject("C001", "English"));
		subjectStore.addSubject(new Subject("C002", "aptitude"));
		subjectStore.addSubject(new Subject("S001", "Biology"));
		subjectStore.addSubject(new Subject("S002", "Maths"));
		subjectStore.addSubject(new Subject("S003", "Chemistry"));
//		subjectStore.addSubject(new Subject("S004", "Physics"));
		
		AlResultService alResl = new AlResultServiceImpl(subjectStore);
			alResl.assignIndexNumber("IN001");
			alResl.setEnglishMarks("C001", 80.0);
			alResl.setAptitudeMarks("C002", 45.0);
			alResl.addMarks("S001", 75.0);
			alResl.addMarks("S002", 45.0);
			alResl.addMarks("S003", 96.0);
//			alResl.addMarks("S004", 55.0);
			alResl.printResult();
			
			AlResultService alResl1 = new AlResultServiceImpl(subjectStore);
			alResl1.assignIndexNumber("IN002");
			alResl1.setEnglishMarks("C001", 52.0);
			alResl1.setAptitudeMarks("C002", 89.0);
			alResl1.addMarks("S001", 25.0);
			alResl1.addMarks("S002", 95.0);
			alResl1.addMarks("S003", 86.0);
//			alResl1.addMarks("S004", 75.0);
			alResl1.printResult();

		
	
	
		

	

	}
}
