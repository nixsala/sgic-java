package examresult.ni;

public class SubjectServiceImpl implements SubjectService{
	
	private Subject[] subjects=new Subject[10];
	private int subjectCounter=0;

	@Override
	public void addSubject(Subject subject) {
		subjects[subjectCounter]=subject;
		subjectCounter++;
		
	}

	@Override
	public Subject getSubbyId(String subjectid) {
		for (int i = 0; i < subjectCounter; i++) {
			if (subjectid.equals(subjects[i].getId())) {
				return subjects[i];
				
				
			}
		}
		return null;
	}

}
