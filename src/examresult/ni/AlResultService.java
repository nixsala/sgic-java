package examresult.ni;

public interface AlResultService {
	public void assignIndexNumber(String indexNumber);

	public void addMarks(String subjectId, Double marks);

	public double getTotal();

	public Double getaverage();

	public Double getStanderdDiveration();

	public void setEnglishMarks(String subjectId, Double marks);

	public void setAptitudeMarks(String subjectId, Double marks);

	public void printResult();

}
