package examresult.ni;

public class AlResultServiceImpl implements AlResultService{
	
	private String indexNumber;
	private SubjectService subjectService;
	
	private Subject[] studentSubject=new Subject[5];
	private Double[] studentMarks=new Double[5];
	
	private int counter=2;
	
	
	public AlResultServiceImpl(SubjectService subjectService) {
		
		this.subjectService = subjectService;
		
	}
		
	@Override
	public void assignIndexNumber(String indexNumber) {
		this.indexNumber=indexNumber;
		
	}
	@Override
	public void addMarks(String subjectId, Double marks) {
		Subject subjectObj = subjectService.getSubbyId(subjectId);
		studentSubject[counter] = subjectObj;
		studentMarks[counter] = marks;
		counter++;
		
		
	}
	@Override
	public double getTotal() {
		double total=0;
		if (counter==5) {
			for (int i = 2; i < counter; i++) {
				total=total+studentMarks[i];
						
				
			}
			
		}
		return total;
	}
	@Override
	public Double getaverage() {
		return getTotal()/3;
	}
	@Override
	public Double getStanderdDiveration() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void printResult() {
		System.out.println("-------------Result_Sheet----------------");
		System.out.println("Index Number : " + this.indexNumber);
		System.out.println("Subject Id \t|\tSubject Name \t|\tMarks ");
		
		for (int i = 0; i < counter; i++) {
			System.out.println(studentSubject[i].getId()+"\t|\t" + studentSubject[i].getName()+"\t|\t"+studentMarks[i]);
			
		}
		
		System.out.println("Total : " + this.getTotal());
		System.out.println("Average : " + this.getaverage());
		
	}
	@Override
	public void setEnglishMarks(String subjectId, Double marks) {
		Subject subObj = subjectService.getSubbyId(subjectId);
		studentSubject[0] = subObj;
		studentMarks[0] = marks;
		
	}
	@Override
	public void setAptitudeMarks(String subjectId, Double marks) {
		Subject subObj = subjectService.getSubbyId(subjectId);
		studentSubject[1] = subObj;
		studentMarks[1] = marks;
		
	}
	
	
	
	

}
